package com.zuitt.wdc044.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

//mark this Java object as a representation of a database table via @Entity
@Entity
//designate the table name
@Table(name = "users")

public class User {
    @Id
    //auto-increment
    @GeneratedValue
    private Long id;

    //class properties that represent a table column
    @Column
    private String username;

    @Column
    private String password;

    //represents the one side of the relationship
    @OneToMany(mappedBy = "user")
    //infinite recursion
    @JsonIgnore
    private Set<Post> posts;

    //default constructor, this is needed when retrieving posts
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //getter and setter
    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }
//    public String getUsername(){
//        return username;
//    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    //getting the posts
    public Set<Post> getPosts(){
        return posts;
    }

}
